package loadBalancing;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet implementation class Servlet
 */
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    Monitor monitor = new Monitor();
    Random rand = new Random();
    Thread th;
    int robin = 0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet() {
        super();
        new Thread(new Monitor()).start();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		handleRequest(request, response);
	}
	
	@SuppressWarnings("unchecked")
	private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// url
    	String url = null;
            
    	while (url == null){
        	if (monitor.healthCount > 0) {
        		robin = (robin + 1) % monitor.healthCount;
        		url = monitor.getUrl(robin);
        	}
        }
        
        HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
        
        // HTTP method
        connection.setRequestMethod("GET");
        
        // copy headers
        ArrayList<String> heads = new ArrayList<String>();
        heads = Collections.list( request.getHeaderNames() );
        for(String head : heads) {
        	connection.setRequestProperty((String)head, request.getHeader((String) head));
        }
        
        // copy body
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        
        String inputLine;
        ServletOutputStream sout = response.getOutputStream();
        System.out.println("URL: " + url);
        while ((inputLine = inputStream.readLine()) != null) {
            sout.write(inputLine.getBytes());
        }
        // close
        inputStream.close();
        sout.flush();
	}
	
	

}
