package loadBalancing;

import java.net.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Monitor implements Runnable {
	
	static ArrayList<String> services;
	static ArrayList<String> healthServices;
	public static int healthCount;

	public Monitor() {
		healthServices = new ArrayList<>();
		services = new ArrayList<String>();
		services.add("http://147.32.233.18:8888/MI-MDW-LastMinute1/list");
	    services.add("http://147.32.233.18:8888/MI-MDW-LastMinute2/list");
	    services.add("http://147.32.233.18:8888/MI-MDW-LastMinute3/list");
	    healthCount = 0;
	}
	
	public static String getUrl(int position){
		if (position < healthServices.size()){
			return healthServices.get(position);
		}
		return null;
	}

	@Override
	public void run() {
		try {
			while (true){
				healthCount = 0;
				healthServices.clear();
				for (String service : services) {
					String url = service;
					HttpURLConnection connection = (HttpURLConnection) (new URL(url)).openConnection();
					connection.setRequestMethod("GET");
					int code = connection.getResponseCode();
					if (code == 200){
						//System.out.println("add");
						healthServices.add(url);
						healthCount++;
					}
				}
				TimeUnit.SECONDS.sleep(5);
			}
		} catch (Exception e) {
			System.out.println("error");
		}
	}
	
}